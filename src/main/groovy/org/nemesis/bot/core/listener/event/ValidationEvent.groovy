package org.nemesis.bot.core.listener.event

import org.nemesis.bot.core.Page
import org.nemesis.bot.service.validation.ValidationCondition

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 9:40 AM
 *
 * Event related to validation
 */
class ValidationEvent implements Event {
    private ValidationCondition condition
    private Boolean positive
    private String expectedValue
    private Page page

    ValidationCondition getCondition() {
        return condition
    }

    Boolean getPositive() {
        return positive
    }

    String getExpectedValue() {
        return expectedValue
    }

    Page getPage() {
        return page
    }
}
