package org.nemesis.bot.util

/**
 * User: aalbul
 * Date: 2/20/13
 * Time: 10:58 AM
 *
 * Utility class that contains data extraction mechanisms
 */
class ExtractionUtil {

    /**
     * Cuts a part of string by starting and ending sequences
     * @param source - source string to cut from
     * @param startingSequence - starting sequence (if not found), the beginning index will be start of the string
     * @param endSequence - ending sequence. If null or not found, it will be the end of the source string
     * @return desired part of the string
     */
    public static String cut(String source, String startingSequence, String endSequence = null) {
        int startPosition = 0
        if (source.indexOf(startingSequence) != -1) {
            startPosition = source.indexOf(startingSequence) + startingSequence.length()
        }

        int endPosition = source.length()
        if (endSequence != null && source.indexOf(endSequence, startPosition) != -1) {
            endPosition = source.indexOf(endSequence, startPosition) - endSequence.length()
        }

        return source.substring(startPosition, endPosition)
    }

    /**
     * Search for the first match of the specified regexp
     * @param source - source string to search in
     * @param regexp - regexp to evaluate
     * @return found match or null if nothing found
     */
    public static String extractByRegexp(String source, String regexp) {
        return source.find(regexp)
    }
}
