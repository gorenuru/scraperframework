package org.nemesis.bot.core.listener.event

import jodd.jerry.Jerry
import org.nemesis.bot.core.Page
import org.nemesis.bot.service.validation.ValidationError

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 9:49 AM
 *
 * Event related to the validation error
 */
class ValidationErrorEvent implements Event {
    private ValidationError errorType
    private String selector
    private String expectedValue
    private Boolean positive = true
    private Page page
    private Integer elementsCount = 0
    private Jerry element

    ValidationError getErrorType() {
        return errorType
    }

    String getSelector() {
        return selector
    }

    String getExpectedValue() {
        return expectedValue
    }

    Boolean getPositive() {
        return positive
    }

    Page getPage() {
        return page
    }

    Integer getElementsCount() {
        return elementsCount
    }

    Jerry getElement() {
        return element
    }
}
