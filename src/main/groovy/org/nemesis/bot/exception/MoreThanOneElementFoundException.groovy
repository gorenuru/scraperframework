package org.nemesis.bot.exception

/**
 * User: aalbul
 * Date: 2/19/13
 * Time: 3:25 PM
 */
class MoreThanOneElementFoundException extends SelectionException {

    MoreThanOneElementFoundException(String selector) {
        super(selector)
    }
}
