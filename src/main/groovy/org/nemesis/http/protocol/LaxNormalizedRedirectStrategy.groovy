package org.nemesis.http.protocol

import org.apache.http.Header
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.ProtocolException
import org.apache.http.impl.client.LaxRedirectStrategy
import org.apache.http.protocol.HttpContext

/**
 * User: nuru
 * Date: 23.12.12
 * Time: 21:53
 *
 * Lax redirection strategy that replace # with ? and give an ability to track URI parameters
 */
class LaxNormalizedRedirectStrategy extends LaxRedirectStrategy {

    @Override
    URI getLocationURI(HttpRequest request, HttpResponse response, HttpContext context) throws ProtocolException {
        //get the location header to find out where to redirect to
        Header locationHeader = response.getFirstHeader("location");
        if (locationHeader == null) {
            // got a redirect response, but no location header
            throw new ProtocolException(
                    "Received redirect response " + response.getStatusLine()
                            + " but no location header");
        }
        String location = locationHeader.getValue();

        URI uri = createLocationURI(location);

        response.removeHeaders("location")
        response.setHeader("location", uri.toString().replaceAll("#", "?"))
        return super.getLocationURI(request, response, context)
    }
}
