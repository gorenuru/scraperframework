package org.nemesis.bot.core.listener.event

import org.nemesis.bot.core.Page
import org.nemesis.http.core.HttpRequestConfig

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 9:27 AM
 *
 * Event that is related to metadata redirection
 */
class RedirectionEvent implements Event {
    private HttpRequestConfig config
    private Page page

    HttpRequestConfig getConfig() {
        return config
    }

    Page getPage() {
        return page
    }
}
