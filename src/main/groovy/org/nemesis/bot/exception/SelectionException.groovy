package org.nemesis.bot.exception

/**
 * User: aalbul
 * Date: 2/19/13
 * Time: 3:25 PM
 */
class SelectionException extends BotException {
    private String selector

    SelectionException(String selector) {
        this.selector = selector
    }
}
