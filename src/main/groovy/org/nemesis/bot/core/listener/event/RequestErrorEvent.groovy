package org.nemesis.bot.core.listener.event

import org.nemesis.http.core.HttpRequestConfig

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 9:21 AM
 *
 * Event related to the HTTP request error
 */
class RequestErrorEvent implements Event {
    private HttpRequestConfig config
    private Exception cause

    HttpRequestConfig getConfig() {
        return config
    }

    Exception getCause() {
        return cause
    }
}
