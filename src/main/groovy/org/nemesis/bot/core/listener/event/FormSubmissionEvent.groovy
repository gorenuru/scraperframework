package org.nemesis.bot.core.listener.event

import jodd.jerry.Jerry
import org.nemesis.http.core.HttpRequestConfig
import org.nemesis.http.domain.RequestMethod

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 9:08 AM
 *
 * Event related to the form submission
 */
class FormSubmissionEvent implements Event {
    private String selector
    private Jerry formElement
    private HttpRequestConfig config

    String getSelector() {
        return selector
    }

    Jerry getFormElement() {
        return formElement
    }

    HttpRequestConfig getConfig() {
        return config
    }
}
