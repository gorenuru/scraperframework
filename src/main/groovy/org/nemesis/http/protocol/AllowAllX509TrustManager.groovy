package org.nemesis.http.protocol

import javax.net.ssl.X509TrustManager
import java.security.cert.CertificateException
import java.security.cert.X509Certificate

/**
 * User: nuru
 * Date: 01.01.13
 * Time: 16:24
 *
 * Implementation of the X509TrustManager that allow an access to any server and certiificate
 */
class AllowAllX509TrustManager implements X509TrustManager {

    @Override
    void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
    }

    @Override
    void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
    }

    @Override
    X509Certificate[] getAcceptedIssuers() {
        return null
    }
}
