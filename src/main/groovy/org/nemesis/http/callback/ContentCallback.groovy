package org.nemesis.http.callback

import org.apache.http.HttpResponse

/**
 * User: nuru
 * Date: 01.01.13
 * Time: 10:44
 */
interface ContentCallback {

    public String callback(HttpResponse response);
}
