package org.nemesis.http.util

/**
 * User: nuru
 * Date: 17.03.13
 * Time: 21:28
 */
class UrlUtil {

    public static String composeUrl(String sourceUrl, Map<String, String> params) {
        StringBuilder builder = new StringBuilder(sourceUrl)
        if (params != null) {
            boolean firstPopulated = sourceUrl.contains("?")
            params.each {
                if (!firstPopulated) {
                    builder.append("?")
                    firstPopulated = true
                } else {
                    builder.append("&")
                }
                builder.append(it.key).append("=").append(URLEncoder.encode(it.value, "UTF-8"))
            }
        }
        return builder.toString()
    }
}
