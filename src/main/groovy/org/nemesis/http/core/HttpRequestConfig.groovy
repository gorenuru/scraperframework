package org.nemesis.http.core

import org.apache.http.entity.ContentType
import org.nemesis.http.callback.BasicContentCallback
import org.nemesis.http.callback.ContentCallback
import org.nemesis.http.domain.RequestMethod

/**
 * User: aalbul
 * Date: 2/19/13
 * Time: 11:25 AM
 */
class HttpRequestConfig {
    private RequestMethod method = RequestMethod.GET
    private String url
    private Map<String, String> headers = new HashMap<>()
    private Map<String, String> params = new HashMap<>()
    private Map<String, String> form = new HashMap<>()
    private String contentType = ContentType.APPLICATION_JSON.mimeType
    private Serializable entity
    private ContentCallback callback = new BasicContentCallback()

    RequestMethod getMethod() {
        return method
    }

    void setMethod(String method) {
        this.method = RequestMethod.valueOf(method.toUpperCase())
    }

    String getUrl() {
        return url
    }

    void setUrl(String url) {
        this.url = url
    }

    Map<String, String> getHeaders() {
        return headers
    }

    void setHeaders(Map<String, String> headers) {
        this.headers = headers
    }

    Map<String, String> getParams() {
        return params
    }

    void setParams(Map<String, String> params) {
        this.params = params
    }

    Map<String, String> getForm() {
        return form
    }

    void setForm(Map<String, String> form) {
        this.form = form
    }

    String getContentType() {
        return contentType
    }

    void setContentType(String contentType) {
        this.contentType = contentType
    }

    Serializable getEntity() {
        return entity
    }

    void setEntity(Serializable entity) {
        this.entity = entity
    }

    ContentCallback getCallback() {
        return callback
    }

    void setCallback(ContentCallback callback) {
        this.callback = callback
    }

    @Override
    public String toString() {
        def result = new StringBuilder()
        result << "\tURL: ${url}\n"
        result << "\tQuery Params: ${params}\n"
        result << "\tForm Data: ${form}\n"
        result << "\tHeaders: ${headers}\n"
        result << "\tContent Type: ${contentType}\n"
        result << "\tEntity: ${entity}\n"
        result << "\tMethod: ${method}"
        return result.toString()
    }
}
