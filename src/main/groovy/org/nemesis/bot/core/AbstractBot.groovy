package org.nemesis.bot.core

import groovy.json.JsonSlurper
import org.apache.http.client.HttpResponseException
import org.nemesis.bot.core.listener.BotEventListener
import org.nemesis.bot.core.listener.LoggingListener
import org.nemesis.bot.core.listener.event.RedirectionEvent
import org.nemesis.bot.core.listener.event.RequestErrorEvent
import org.nemesis.bot.exception.RequestException
import org.nemesis.bot.service.validation.AbstractValidator
import org.nemesis.bot.service.validation.ValidationCondition
import org.nemesis.http.HttpTemplate
import org.nemesis.http.core.HttpRequestConfig
import org.nemesis.http.domain.HttpTemplateResponse

/**
 * User: aalbul
 * Date: 2/28/13
 * Time: 9:48 AM
 */
abstract class AbstractBot<T> {
    protected AbstractValidator validator
    protected BotEventListener listener = new LoggingListener()
    protected JsonSlurper jsonParser

    protected Page page

    AbstractBot(AbstractValidator validator) {
        this.validator = validator
        jsonParser = new JsonSlurper()
    }

    /**
     * Navigates to specified page
     * @param page - page link
     * @param method - request method. Default is GET
     * @param queryParams - query link parameters
     */
    public abstract void navigate(Closure configurationClosure)

    /**
     * Performs the form submission by specified form selector
     *
     * How it works?
     * First, we are collecting all the inputs on the form and preparing the request map
     * Then, we override form values with specified user values
     *
     * The last step is the actual submission (taking into account method and action from the form element)
     *
     * @param formSelector - form CSS selector
     * @param userFormValues - user form values that will be overridden
     */
    public abstract void submit(String formSelector, Map<String, String> userFormValues)

    /**
     * Performs click operation on specified element (currently, only "a" elements are expected)
     * @param selector - "a" element selector
     */
    public abstract void click(String selector)

    /**
     * Search for the single element by selector
     * @param selector - selector to search for
     * @param expectedType - expected type. If specified, an additional validation will be applied
     * @return found element
     */
    public abstract T $(String selector, String expectedType)

    /**
     * Search for multiple elements by selector
     * @param selector - selector to search for
     * @param expectedType - expected type. If specified, an additional validation will be applied
     * @return found elements
     */
    public abstract T $$(String selector, String expectedType)

    /**
     * Parse content URI from the meta tag (if exists)
     * @return URI instance or null if no redirect URI found
     */
    protected abstract URI extractRedirectUriFromMetaTag()

    /**
     * Method to safely execute Http request.
     * It will handle all the corner cases and log proper data
     * @param request - request to execute
     * @param process - indicates if we need to process the response
     * @return response instance
     * @throws RequestException - if any exception is happened during request
     */
    public HttpTemplateResponse executeRequest(HttpRequestConfig config, boolean process = true, Integer retryCount = 5,
                                               Integer currentTry = 0) throws RequestException {
        try {
            HttpTemplateResponse response = HttpTemplate.instance.execute(config)
            if (process) {
                processResponse(response)
            }
            return response
        } catch (Exception e) {
            if (currentTry < retryCount) {
                return executeRequest(config, process, retryCount, currentTry + 1)
            } else {
                fillAndThrowException(config, e)
            }
        }
    }

    /**
     * Method to safely execute Http request.
     * It will handle all the corner cases and log proper data
     * @param configClosure - closure that contains configuration
     * @param process - indicates if we need to process the response
     * @return response instance
     * @throws RequestException - if any exception is happened during request
     */
    public HttpTemplateResponse executeRequest(Closure configClosure, boolean process = true, Integer retryCount = 5,
                                               Integer currentTry = 0) throws RequestException {
            HttpRequestConfig config = HttpTemplate.instance.extractConfig(configClosure)
        return executeRequest(config, process, retryCount, currentTry)
    }

    /**
     * Fill an exception and throw it
     * @param request - source request instance
     * @param e - root exception
     */
    private void fillAndThrowException(HttpRequestConfig request, Exception e) {
        Integer errorCode = null
        if (e.class.isAssignableFrom(HttpResponseException)) {
            errorCode = (e as HttpResponseException).statusCode
        }
        listener?.handleEvent(new RequestErrorEvent(
                config: request,
                cause: e,
        ))
        throw new RequestException(request, e, errorCode)
    }

    /**
     * Converts response body to json instance
     * @param content - content to convert
     * @return json instance
     */
    public toJson(String content) {
        return jsonParser.parse(new StringReader(content))
    }

    /**
     * Converts response body to json instance
     * @param response - response instance to convert
     * @return json instance
     */
    public toJson(HttpTemplateResponse response) {
        return toJson(response.body)
    }

    /**
     * Verify if specified condition is "true"
     * Otherwise, error will be thrown
     * @param condition - condition to verify
     * @param value - value to apply to verification
     */
    protected void checkIf(ValidationCondition condition, String value) {
        validator.checkCondition(condition, value, true, page, listener)
    }

    /**
     * Verify if specified condition is "false"
     * Otherwise, error will be thrown
     * @param condition - condition to verify
     * @param value - value to apply to verification
     */
    protected void checkIfNot(ValidationCondition condition, String value) {
        validator.checkCondition(condition, value, false, page, listener)
    }

    /**
     * Prepares the URI according to the specified one.
     * If specified element is not absolute, it will be returned as - is
     * Otherwise, we will compose a new URL that is related to the current page
     *
     * @param url - source url
     * @return absolute url
     */
    protected String prepareAbsoluteUrl(String url) {
        if (url?.trim()?.isEmpty()) {
            return page.uri.toASCIIString()
        } else if (url.toLowerCase().contains("http")) {
            return url
        } else {
            return "${page.uri.scheme}://${page.uri.host}${page.url.endsWith("/") ? '' : '/'}${url}"
        }
    }

    /**
     * Process result returned by the RestTemplate navigation.
     * This method is simply swap the state of the page to the new one
     * @param response - response to process
     */
    public void processResponse(HttpTemplateResponse response) {
        page = new Page(response)

        def redirectUrl = extractRedirectUriFromMetaTag()
        if (redirectUrl != null) {
            def config = new HttpRequestConfig(url: redirectUrl)
            listener?.handleEvent(new RedirectionEvent(
                    config: config,
                    page: page
            ))
            processResponse(executeRequest(config))
        }
    }

    void setListener(BotEventListener listener) {
        this.listener = listener
    }

    Page getPage() {
        return page
    }
}
