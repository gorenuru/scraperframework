package org.nemesis.bot.service.validation

/**
 * User: aalbul
 * Date: 2/20/13
 * Time: 2:29 PM
 */
public enum ValidationCondition {
    TITLE_CONTAINS,
    HTML_CONTAINS,
    URL_CONTAINS,
    ELEMENTS_EXISTS
}