package org.nemesis.bot.core.listener.event

import org.nemesis.http.core.HttpRequestConfig

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 9:03 AM
 *
 * Event related to the navigation state
 */
class NavigationEvent implements Event {
    private HttpRequestConfig config

    HttpRequestConfig getConfig() {
        return config
    }
}
