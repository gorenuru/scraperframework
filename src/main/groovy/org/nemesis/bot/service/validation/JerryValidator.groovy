package org.nemesis.bot.service.validation

import org.nemesis.bot.core.listener.BotEventListener
import org.nemesis.bot.core.Page
import org.nemesis.bot.core.listener.event.ValidationErrorEvent
import org.nemesis.bot.exception.ExtraElementFoundException
import org.nemesis.bot.exception.InvalidElementTypeException
import org.nemesis.bot.exception.MoreThanOneElementFoundException
import org.nemesis.bot.exception.NoElementsFoundException
import org.nemesis.bot.exception.ValidationException

/**
 * User: aalbul
 * Date: 2/20/13
 * Time: 9:28 AM
 *
 * Jerry - specific validator implementation
 */
class JerryValidator extends AbstractValidator {

    @Override
    public void verifySingleSelector(String selector, Page page, BotEventListener listener) throws NoElementsFoundException, MoreThanOneElementFoundException {
        verifyMultipleSelector(selector, true, page, listener)
        def selectorResult = page.content.$(selector)
        if (selectorResult.size() > 1) {
            listener?.handleEvent(new ValidationErrorEvent(
                    expectedValue: selector,
                    page: page,
                    errorType: ValidationError.MORE_THAN_ONE_ELEMENT,
                    elementsCount: selectorResult.size()
            ))
            throw new MoreThanOneElementFoundException(selector)
        }
    }

    @Override
    public void verifyMultipleSelector(String selector, Boolean exists, Page page, BotEventListener listener) throws NoElementsFoundException {
        def selectorResult = page.content.$(selector)

        if (exists && selectorResult.size() == 0) {
            listener?.handleEvent(new ValidationErrorEvent(
                    expectedValue: selector,
                    page: page,
                    errorType: ValidationError.ELEMENT_NOT_FOUND,
                    elementsCount: selectorResult.size()
            ))
            throw new NoElementsFoundException(selector)
        } else if (!exists && selectorResult.size() > 0) {
            listener?.handleEvent(new ValidationErrorEvent(
                    expectedValue: selector,
                    page: page,
                    errorType: ValidationError.EXTRA_ELEMENT_FOUND,
                    elementsCount: selectorResult.size()
            ))
            throw new ExtraElementFoundException()
        }
    }

    @Override
    public void verifyElementType(String selector, String expectedType, Page page, BotEventListener listener) throws InvalidElementTypeException {
        def selectorResult = page.content.$(selector)
        if (!selectorResult.is(expectedType) && selectorResult.get().length > 0) {
            listener?.handleEvent(new ValidationErrorEvent(
                    expectedValue: expectedType,
                    selector: selector,
                    page: page,
                    errorType: ValidationError.INVALID_TYPE,
                    elementsCount: selectorResult.size(),
                    element: selectorResult
            ))
            throw new InvalidElementTypeException(selector, selectorResult.get()[0].getNodeName(), expectedType)
        }
    }

    @Override
    public void verifyContainsText(String source, String desiredPart, Boolean contains, BotEventListener listener) {
        def reason
        def errorType = null
        if (contains && !source.toLowerCase().contains(desiredPart.toLowerCase())) {
            reason = "Error. Can't find text part: ${desiredPart}."
            errorType = ValidationError.TEXT_PART_NOT_FOUND
        } else if (!contains && source.toLowerCase().contains(desiredPart.toLowerCase())) {
            reason = "Error. Text was not expected but found: ${desiredPart}."
            errorType = ValidationError.TEXT_PART_FOUND
        }
        if (reason) {
            listener?.handleEvent(new ValidationErrorEvent(
                    expectedValue: desiredPart,
                    errorType: errorType,
            ))
            throw new ValidationException(reason)
        }
    }
}
