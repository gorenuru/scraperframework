package org.nemesis.bot.core.listener

import org.nemesis.bot.core.listener.event.Event

/**
 * User: aalbul
 * Date: 2/20/13
 * Time: 9:30 AM
 *
 * The main listener interface
 */
public interface BotEventListener {

    /**
     * Handle passed event
     * @param event - event to handle
     */
    public void handleEvent(Event event)
}