package org.nemesis.http.domain;

import java.net.URI;

/**
 * User: nuru
 * Date: 01.01.13
 * Time: 10:27
 */
public class HttpTemplateResponse {
    String body;
    URI uri;

    public String getBody() {
        return body;
    }

    public URI getUri() {
        return uri;
    }
}
