package org.nemesis.bot.service.validation

import org.nemesis.bot.core.listener.BotEventListener
import org.nemesis.bot.core.Page
import org.nemesis.bot.core.listener.event.ValidationEvent
import org.nemesis.bot.exception.ExtraElementFoundException
import org.nemesis.bot.exception.InvalidElementTypeException
import org.nemesis.bot.exception.MoreThanOneElementFoundException
import org.nemesis.bot.exception.NoElementsFoundException

import static org.nemesis.bot.service.validation.ValidationCondition.*

/**
 * User: aalbul
 * Date: 2/28/13
 * Time: 9:50 AM
 *
 * Base class for the validation purposes
 */
abstract class AbstractValidator {

    /**
     * Verify that specified selector returns only one element
     * @param selector - selector to verify
     * @param page - page to select from
     * @param listener - listener to notify (may be null)
     * @throws org.nemesis.bot.exception.NoElementsFoundException - if no elements found
     * @throws org.nemesis.bot.exception.MoreThanOneElementFoundException - if more than one element found
     */
    public abstract void verifySingleSelector(String selector, Page page, BotEventListener listener) throws NoElementsFoundException, MoreThanOneElementFoundException

    /**
     * Verify that specified selector returns at least one element
     * @param selector - selector to verify
     * @param exists - reverse the validation to check 2 kinds of condition: exists and not exists
     * @param page - page to select from
     * @param listener - listener to notify (may be null)
     * @throws NoElementsFoundException - if no elements found
     * @throws ExtraElementFoundException - if no elements expected
     */
    public abstract void verifyMultipleSelector(String selector, Boolean exists, Page page, BotEventListener listener) throws NoElementsFoundException, ExtraElementFoundException

    /**
     * Verify found element type
     * @param selector - selector to verify
     * @param expectedType - expected type of element (form, input e.t.c.)
     * @param page - page to select from
     * @param listener - listener to notify (may be null)
     * @throws org.nemesis.bot.exception.InvalidElementTypeException - if element type is invalid
     */
    public abstract void verifyElementType(String selector, String expectedType, Page page, BotEventListener listener) throws InvalidElementTypeException

    /**
     * Verify current title. This method verify contains / not contains conditions according to "contains" parameter state
     * @param titlePart - part of title to verify according to
     * @param contains - the path of validation
     * @param page - current page instance
     * @param listener - listener instance (may be null)
     */
    public abstract void verifyContainsText(String source, String desiredPart, Boolean contains, BotEventListener listener)

    /**
     * Validates condition
     * @param condition - condition to verify
     * @param value - value to apply to the validation
     * @param positive - true if we need to verify positive flow
     * @param page - current page instance
     * @param listener - listener instance (may be null)
     */
    public void checkCondition(ValidationCondition condition, String value, Boolean positive, Page page, BotEventListener listener) {
        switch (condition) {
            case ELEMENTS_EXISTS:
                verifyMultipleSelector(value, positive, page, listener)
                break
            case TITLE_CONTAINS:
                verifyContainsText(page.title, value, positive, listener)
                break
            case HTML_CONTAINS:
                verifyContainsText(page.html, value, positive, listener)
                break
            case URL_CONTAINS:
                verifyContainsText(page.url, value, positive, listener)
                break
        }
        listener?.handleEvent(new ValidationEvent(
                condition: condition,
                positive: positive,
                expectedValue: value,
                page: page
        ))
    }
}
