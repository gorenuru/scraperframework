package org.nemesis.bot.core.listener

import org.apache.http.client.HttpResponseException
import org.nemesis.bot.core.listener.event.ClickEvent
import org.nemesis.bot.core.listener.event.Event
import org.nemesis.bot.core.listener.event.FormSubmissionEvent
import org.nemesis.bot.core.listener.event.NavigationEvent
import org.nemesis.bot.core.listener.event.RedirectionEvent
import org.nemesis.bot.core.listener.event.RequestErrorEvent
import org.nemesis.bot.core.listener.event.ValidationErrorEvent
import org.nemesis.bot.core.listener.event.ValidationEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.nemesis.bot.service.validation.ValidationCondition.*
import static org.nemesis.bot.service.validation.ValidationError.*

/**
 * User: aalbul
 * Date: 2/28/13
 * Time: 11:24 AM
 */
class LoggingListener implements BotEventListener {
    private final static Logger logger = LoggerFactory.getLogger(LoggingListener.class)

    @Override
    void handleEvent(Event event) {
        internalHandle(event)
    }

    private void internalHandle(NavigationEvent event) {
        logger.info("Navigating to: [${event.config.url}], method: [${event.config.method}], query parameters: [${event.config.params}]")
    }

    private void internalHandle(FormSubmissionEvent event) {
        logger.info("Submiting form: [${event.selector}], with method: [${event.config.method}], action link: [${event.config.url}], params: [${event.config.form}]")
    }

    private void internalHandle(ClickEvent event) {
        logger.info("Clicking on the link: ${event.config.url}")
    }

    private void internalHandle(RequestErrorEvent event) {
        Integer errorCode = null
        String errorMessage = null
        if (event.cause.class.isAssignableFrom(HttpResponseException)) {
            errorCode = (event.cause as HttpResponseException).statusCode
            errorMessage = (event.cause as HttpResponseException).getLocalizedMessage()
        }

        def eventMessage = new StringBuilder()
        eventMessage << "Error. Can't proceed with request:\n"
        eventMessage << "${event.config}\n"
        eventMessage << "\tError code: ${errorCode}\n"
        eventMessage << "\tError message: ${errorMessage}\n"
        eventMessage << "\tException class: ${event.cause.getClass().getCanonicalName()}"

        logger.info(eventMessage.toString())
    }

    private void internalHandle(RedirectionEvent event) {
        logger.info("Redirecting by meta tag to: ${event.config.url}")
    }

    private void internalHandle(ValidationEvent event) {
        switch (event.condition) {
            case ELEMENTS_EXISTS:
                logger.info("Checking if selector [${event.expectedValue}]${event.positive? '' : ' do not'} return results.")
                break
            case TITLE_CONTAINS:
                logger.info("Checking if title${event.positive ? '' : ' do not'} contain: [${event.expectedValue}]. Current title is: [${event.page.title}]")
                break
            case HTML_CONTAINS:
                logger.info("Checking if HTML${event.page ? '' : ' do not'} contain: [${event.expectedValue}].")
                break
            case URL_CONTAINS:
                logger.info("Checking if URL${event.positive ? '' : ' do not'} contain: [${event.expectedValue}]. Current URL is: [${event.page.url}]")
                break
        }
    }

    private void internalHandle(ValidationErrorEvent event) {
        switch (event.errorType) {
            case MORE_THAN_ONE_ELEMENT:
                logger.error("Found more than one result for: [${event.expectedValue}]")
                break
            case ELEMENT_NOT_FOUND:
                logger.error("No elements found for: [${event.expectedValue}]")
                break
            case EXTRA_ELEMENT_FOUND:
                logger.error("Expected 0 elements but found ${event.elementsCount} for [${event.expectedValue}]")
                break
            case INVALID_TYPE:
                logger.error("Expected type: [${event.expectedValue}] but found [${event.element.get()[0].nodeName}] for selector: [${event.selector}]")
                break
            case TEXT_PART_FOUND:
                logger.error("Text part: [${event.expectedValue}] found but was not expected")
                break
            case TEXT_PART_NOT_FOUND:
                logger.error("Expected text part: [${event.expectedValue}] not found")
                break
        }
    }

    private void internalHandle(Event event) {
        logger.info("Unknown event specified")
    }
}
