package org.nemesis.http.domain;

/**
 * User: nuru
 * Date: 01.01.13
 * Time: 10:28
 */
public enum RequestMethod {
    GET,
    POST,
    PUT,
    DELETE
}
