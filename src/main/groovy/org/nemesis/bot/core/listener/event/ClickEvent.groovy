package org.nemesis.bot.core.listener.event

import jodd.jerry.Jerry
import org.nemesis.http.core.HttpRequestConfig

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 9:16 AM
 *
 * Event related to the link clicking
 */
class ClickEvent implements Event {
    private String selector
    private Jerry element
    private HttpRequestConfig config

    String getSelector() {
        return selector
    }

    Jerry getElement() {
        return element
    }

    HttpRequestConfig getConfig() {
        return config
    }
}
