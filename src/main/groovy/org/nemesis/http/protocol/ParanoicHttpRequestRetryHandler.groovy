package org.nemesis.http.protocol

import org.apache.http.impl.client.StandardHttpRequestRetryHandler
import org.apache.http.protocol.HttpContext

/**
 * Created with IntelliJ IDEA.
 * User: nuru
 * Date: 23.12.12
 * Time: 15:56
 * The paranoic implementation of HttpRequestRetryHander which will retry on any kind of exception
 */
class ParanoicHttpRequestRetryHandler extends StandardHttpRequestRetryHandler {

    ParanoicHttpRequestRetryHandler(int retryCount) {
        super(retryCount, false)
    }

    @Override
    boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
        return  executionCount > this.retryCount;
    }
}
