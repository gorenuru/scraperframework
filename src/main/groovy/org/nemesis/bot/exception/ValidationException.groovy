package org.nemesis.bot.exception

/**
 * User: aalbul
 * Date: 2/20/13
 * Time: 4:02 PM
 */
class ValidationException extends BotException {
    private String reason;

    ValidationException(String reason) {
        this.reason = reason
    }
}
