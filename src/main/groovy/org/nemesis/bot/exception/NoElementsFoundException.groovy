package org.nemesis.bot.exception

/**
 * User: aalbul
 * Date: 2/19/13
 * Time: 3:23 PM
 */
class NoElementsFoundException extends SelectionException {

    NoElementsFoundException(String selector) {
        super(selector)
    }
}
