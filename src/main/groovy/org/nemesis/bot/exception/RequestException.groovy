package org.nemesis.bot.exception

import org.nemesis.http.core.HttpRequestConfig

/**
 * User: aalbul
 * Date: 2/20/13
 * Time: 9:47 AM
 *
 * This exception is thrown during low-level http request
 */
class RequestException extends BotException {
    private HttpRequestConfig failedRequest
    private Exception cause
    private Integer errorCode

    RequestException(HttpRequestConfig failedRequest, Exception cause, Integer errorCode) {
        super(cause)
        this.failedRequest = failedRequest
        this.cause = cause
        this.errorCode = errorCode
    }
}
