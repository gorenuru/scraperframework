package org.nemesis.bot.exception

/**
 * User: aalbul
 * Date: 2/19/13
 * Time: 3:24 PM
 */
class InvalidElementTypeException extends SelectionException {
    private String foundType
    private String expectedType

    InvalidElementTypeException(String selector, String foundType, String expectedType) {
        super(selector)
        this.foundType = foundType
        this.expectedType = expectedType
    }
}
