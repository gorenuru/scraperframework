package org.nemesis.http.callback

import org.apache.http.HttpResponse
import org.apache.http.client.ResponseHandler
import org.apache.http.impl.client.BasicResponseHandler

/**
 * User: aalbul
 * Date: 2/28/13
 * Time: 1:52 PM
 *
 * Basic content callback that just consume data
 */
class BasicContentCallback implements ContentCallback {
    private ResponseHandler<String> responseHandler = new BasicResponseHandler()

    @Override
    String callback(HttpResponse response) {
        return responseHandler.handleResponse(response)
    }
}
