package org.nemesis.bot.service.validation

/**
 * User: aalbul
 * Date: 4/1/13
 * Time: 10:00 AM
 */
enum ValidationError {
    MORE_THAN_ONE_ELEMENT,
    ELEMENT_NOT_FOUND,
    EXTRA_ELEMENT_FOUND,
    INVALID_TYPE,
    TEXT_PART_NOT_FOUND,
    TEXT_PART_FOUND
}
