package org.nemesis.bot.exception

/**
 * User: aalbul
 * Date: 2/19/13
 * Time: 3:23 PM
 */
class BotException extends RuntimeException {
    BotException() {
    }

    BotException(String s) {
        super(s)
    }

    BotException(String s, Throwable throwable) {
        super(s, throwable)
    }

    BotException(Throwable throwable) {
        super(throwable)
    }
}
