package org.nemesis.bot.core

import jodd.jerry.Jerry
import org.nemesis.http.domain.HttpTemplateResponse

import static jodd.jerry.Jerry.*

/**
 * User: aalbul
 * Date: 2/20/13
 * Time: 3:52 PM
 *
 * Container for current page state
 */
class Page {
    Jerry content
    URI uri
    String url
    String html
    String title

    Page(HttpTemplateResponse response) {
        def cuttedBody = response.body.replaceAll("<!DOCTYPE((.|\n|\r)*?)\">", "")
        try {
            this.content = jerry(cuttedBody)
        } catch (Exception e) {
            this.content = jerry("")
        }
        this.uri = response.uri
        this.url = response.uri.toASCIIString()
        this.html = cuttedBody
        this.title = content.$("title").text()
    }

    Jerry getContent() {
        return content
    }

    URI getUri() {
        return uri
    }

    String getUrl() {
        return url
    }

    String getHtml() {
        return html
    }

    String getTitle() {
        return title
    }
}
