package org.nemesis.http

import org.apache.http.HttpHost
import org.apache.http.HttpResponse
import org.apache.http.client.CookieStore
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.client.params.ClientPNames
import org.apache.http.client.params.CookiePolicy
import org.apache.http.client.protocol.ClientContext
import org.apache.http.conn.ClientConnectionManager
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.scheme.SchemeRegistry
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.impl.client.*
import org.apache.http.impl.conn.PoolingClientConnectionManager
import org.apache.http.params.CoreProtocolPNames
import org.apache.http.protocol.BasicHttpContext
import org.apache.http.protocol.ExecutionContext
import org.apache.http.protocol.HttpContext
import org.apache.http.util.EntityUtils
import org.nemesis.http.core.HttpRequestBuilder
import org.nemesis.http.core.HttpRequestConfig
import org.nemesis.http.domain.HttpTemplateResponse
import org.nemesis.http.protocol.AllowAllX509TrustManager
import org.nemesis.http.protocol.LaxNormalizedRedirectStrategy
import org.nemesis.http.protocol.ParanoicHttpRequestRetryHandler

import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager

/**
 * User: aalbul
 * Date: 12/20/12
 * Time: 11:11 AM
 * The template class to perform an HTTP related operations
 */

class HttpTemplate {
    private final static HttpTemplate INSTANCE = new HttpTemplate()
    private HttpRequestBuilder requestBuilder = new HttpRequestBuilder()
    private DefaultHttpClient client
    private ThreadLocal<HttpContext> context = new InheritableThreadLocal<>()

    HttpTemplate() {
        client = new DefaultHttpClient(new PoolingClientConnectionManager())
        client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH)
        client.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true)
        client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17")
        client.setRedirectStrategy(new LaxNormalizedRedirectStrategy())
        client.setHttpRequestRetryHandler(new ParanoicHttpRequestRetryHandler(3))

        SSLContext sslContext = SSLContext.getInstance("TLS");
        TrustManager[] trustManager = [new AllowAllX509TrustManager()]
        sslContext.init(null, trustManager, null);
        SSLSocketFactory socketFactory = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        ClientConnectionManager ccm = client.getConnectionManager();
        SchemeRegistry schemaRegistry = ccm.getSchemeRegistry();
        schemaRegistry.register(new Scheme("https", socketFactory, 443));
    }

    public static HttpTemplate getInstance() {
        return INSTANCE
    }

    /**
     * Initialize and return thread-local context instance
     * @return prepared context instance
     */
    private HttpContext getContext() {
        if (context.get() == null) {
            context.set(new BasicHttpContext())
            CookieStore cookieStore = new BasicCookieStore()
            context.get().setAttribute(ClientContext.COOKIE_STORE, cookieStore)
        }
        return context.get()
    }

    /**
     * Executes specified request and fire callback when response is reached
     * @param configurationClosure - closure that contains the configuration
     * @return template response
     */
    public HttpTemplateResponse execute(Closure configurationClosure) {
        HttpRequestConfig config = extractConfig(configurationClosure)
        return execute(config)
    }

    /**
     * Executes specified request and fire callback when response is reached
     * @param config - request configuration instance
     * @return template response
     */
    public HttpTemplateResponse execute(HttpRequestConfig config) {
        HttpRequestBase request = requestBuilder.buildRequest(config)
        HttpResponse response = client.execute(request, getContext())

        def currentReq = (HttpUriRequest) getContext().getAttribute(ExecutionContext.HTTP_REQUEST);
        def currentHost = (HttpHost)  getContext().getAttribute(ExecutionContext.HTTP_TARGET_HOST);
        String currentUrl = (currentReq.getURI().isAbsolute()) ? currentReq.getURI().toString() : (currentHost.toURI() + currentReq.getURI())

        String result = null;
        try {
            if (config.callback) {
                result = config.callback.callback(response)
            }
        } finally {
            EntityUtils.consume(response.entity);
        }
        return new HttpTemplateResponse(
                body: result,
                uri: currentUrl.toURI()
        )
    }

    /**
     * Extracts configuration from the closure
     * @param configurationClosure - closure that contains the configuration
     * @return ready-to-use configuration instance
     */
    public HttpRequestConfig extractConfig(Closure configurationClosure) {
        HttpRequestConfig config = new HttpRequestConfig()
        configurationClosure.setDelegate(config)
        configurationClosure.setResolveStrategy(Closure.DELEGATE_FIRST)
        configurationClosure.call()
        return config
    }
}
