package org.nemesis.http.core

import org.apache.http.HttpEntity
import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpDelete
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpPut
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.entity.ContentType
import org.apache.http.entity.SerializableEntity
import org.apache.http.message.BasicNameValuePair
import org.nemesis.http.domain.RequestMethod
import org.nemesis.http.util.UrlUtil

/**
 * User: nuru
 * Date: 13.03.13
 * Time: 22:16
 */
class HttpRequestBuilder {

    /**
     * Build request according to the specified configuration
     * @param config - configuration instance
     * @return built HttpRequestBase instance
     */
    public HttpRequestBase buildRequest(HttpRequestConfig config) {
        HttpRequestBase request = create(config)
        setUrl(request, config)
        fillHeaders(request, config)
        populateHttpEntity(request, config)
        return request
    }

    /**
     * Creates HttpRequestBase instance according to the RequestMethod type
     * @param config - config containing request method declaration
     * @return build request instance
     */
    private HttpRequestBase create(HttpRequestConfig config) {
        switch (config.method) {
            case RequestMethod.GET:
                return new HttpGet()
            case RequestMethod.POST:
                return new HttpPost()
            case RequestMethod.PUT:
                return new HttpPut()
            case RequestMethod.DELETE:
                return new HttpDelete()
        }
    }

    /**
     * Fills request headers
     * @param request - request instance
     * @param config - configuration instance
     */
    private void fillHeaders(HttpRequestBase request, HttpRequestConfig config) {
        config.headers.each {
            request.setHeader(it.key, it.value)
        }
    }

    /**
     * Generate and set url for the specified request
     * if params specified, we will build a proper url with this parameters
     * @param request - request instance to fill
     * @param config - configuration instance
     */
    private void setUrl(HttpRequestBase request, HttpRequestConfig config) {
        request.setURI(new URI(UrlUtil.composeUrl(config.url, config.params)))
    }

    /**
     * Populates request entity
     * @param request - request instance
     * @param config - configuration instance
     */
    private void populateHttpEntity(HttpRequestBase request, HttpRequestConfig config) {
        if (request instanceof HttpEntityEnclosingRequestBase) {
            HttpEntity entity
            if (config.entity) {
                entity = new SerializableEntity(config.entity, true)
                entity.setContentType(config.contentType)
            } else {
                List<NameValuePair> postParams = new ArrayList<>()
                config.form.entrySet().each {
                    postParams.add(new BasicNameValuePair(it.key, it.value))
                }
                entity = new UrlEncodedFormEntity(postParams, "UTF-8")
                entity.setContentType(ContentType.APPLICATION_FORM_URLENCODED.mimeType)
            }
            request.setEntity(entity)
        }
    }
}
