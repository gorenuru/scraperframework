package org.nemesis.bot.core

import jodd.jerry.Jerry
import jodd.jerry.JerryFunction
import org.nemesis.bot.core.listener.event.ClickEvent
import org.nemesis.bot.core.listener.event.FormSubmissionEvent
import org.nemesis.bot.core.listener.event.NavigationEvent
import org.nemesis.bot.service.validation.JerryValidator
import org.nemesis.http.HttpTemplate
import org.nemesis.http.core.HttpRequestConfig

import static org.apache.commons.lang3.StringUtils.*;

/**
 * User: aalbul
 * Date: 2/18/13
 * Time: 3:47 PM
 *
 * Jerry - based implementation of bot
 */
class JerryBot extends AbstractBot<Jerry> {
    static {
        //Overriding Jerry's each function
        Jerry.metaClass.each = { Closure<Jerry> closure ->
            def jerry = delegate as Jerry
            jerry.each(new JerryFunction() {
                @Override
                boolean onNode(Jerry $this, int index) {
                    closure.call($this)
                    return true
                }
            })
        }

        //Overriding Jerry's each function
        Jerry.metaClass.eachWithIndex = { Closure<Jerry> closure ->
            def jerry = delegate as Jerry
            jerry.each(new JerryFunction() {
                @Override
                boolean onNode(Jerry $this, int index) {
                    closure.call($this, index)
                    return true
                }
            })
        }
    }

    JerryBot() {
        super(new JerryValidator())
    }

    /**
     * Navigates to specified page
     * @param page - page link
     * @param method - request method. Default is GET
     * @param queryParams - query link parameters
     */
    public void navigate(Closure configurationClosure) {
        HttpRequestConfig config = HttpTemplate.instance.extractConfig(configurationClosure)
        listener?.handleEvent(new NavigationEvent(config: config))
        executeRequest(config)
    }

    public void navigate(String pageUrl) {
        navigate {
            url = pageUrl
        }
    }

    /**
     * Performs the form submission by specified form selector
     *
     * How it works?
     * First, we are collecting all the inputs on the form and preparing the request map
     * Then, we override form values with specified user values
     *
     * The last step is the actual submission (taking into account method and action from the form element)
     *
     * @param formSelector - form CSS selector
     * @param userFormValues - user form values that will be overridden
     */
    public void submit(String formSelector, Map<String, String> userFormValues = new HashMap<>()) {
        def formElement = $(formSelector, "form")
        def formData = getFormData(formSelector, userFormValues)

        def requestMethod = trimToNull(formElement.attr("method"))
        def action = prepareAbsoluteUrl(formElement.attr("action"))

        def requestConfig = HttpTemplate.instance.extractConfig {
            url = action
            if (requestMethod) {
                method = requestMethod
            }
            params = formData
            form = formData
        }

        listener?.handleEvent(new FormSubmissionEvent(
                selector: formSelector,
                formElement: formElement,
                config: requestConfig
        ))
        executeRequest requestConfig
    }

    /**
     * Performs click operation on specified element (currently, only "a" elements are expected)
     * @param selector - "a" element selector
     */
    public void click(String selector) {
        def link = $(selector, "a")
        def href = prepareAbsoluteUrl(link.attr("href"))
        def requestConfig = HttpTemplate.instance.extractConfig {
            url = href
            method = 'GET'
        }

        listener?.handleEvent(new ClickEvent(
                selector: selector,
                element: link,
                config: requestConfig
        ))

        executeRequest requestConfig
    }

    /**
     * Search for the single element by selector
     * @param selector - selector to search for
     * @param expectedType - expected type. If specified, an additional validation will be applied
     * @return found element
     */
    public Jerry $(String selector, String expectedType = null) {
        def result = page.content.$(selector)
        validator.verifySingleSelector(selector, page, listener)
        if (expectedType) {
            validator.verifyElementType(selector, expectedType, page, listener)
        }
        return result
    }

    /**
     * Search for multiple elements by selector
     * @param selector - selector to search for
     * @param expectedType - expected type. If specified, an additional validation will be applied
     * @return found elements
     */
    public Jerry $$(String selector, String expectedType = null) {
        def result = page.content.$(selector)
        if (expectedType) {
            validator.verifyElementType(selector, expectedType, page, listener)
        }
        return result
    }

    protected URI extractRedirectUriFromMetaTag() {
        URI result = null
        validator.verifyElementType('meta[http-equiv="refresh"]', "meta", page, listener)
        def metaTag = page.content.$('meta[http-equiv="refresh"]')
        try {
            if (metaTag.size() == 1) {
                def content = metaTag.attr("content").replaceAll("0; url=", "").replaceAll("'", "")
                result = new URI(content)
            }
        } catch (Exception e) {
            //swallow
        }
        return result
    }

    /**
     * Collects form data from the specified form
     * This method will compose the map of "input" and "textarea" elements in format
     * name/value
     * @param formSelector - form selector
     * @param userValues - user values that will override form data
     * @return composed map
     */
    protected Map<String, String> getFormData(String formSelector, Map<String, String> userValues) {
        Map<String, String> result = [:]
        def form = $(formSelector, "form")
        def formData = form.$("input, textarea")
        formData.each {
            def name = it.attr("name")
            def value = it.attr("value")
            if (name != null && value != null) {
                result.put(name, value)
            }
        }

        //Populating selects
        def selects = form.$("select")
        selects.each {
            def selected = it.$("option:selected")
            if (selected.attr("value") != null) {
                result.put(it.attr("name"), selected.attr("value"))
            }
        }

        result.putAll(userValues)
        return result
    }

}
